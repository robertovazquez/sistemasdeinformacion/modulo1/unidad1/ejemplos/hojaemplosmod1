﻿DROP DATABASE IF EXISTS ejemplo6mod1;
CREATE DATABASE ejemplo6mod1;
USE ejemplo6mod1;



CREATE OR REPLACE TABLE productos ( 
  idproducto int,
  nombreproducto varchar (20),
  peso varchar (20),
  PRIMARY KEY (idproducto)
  
 
);


CREATE OR REPLACE TABLE clientes(
  idcliente int,
  nombre varchar (20),
  PRIMARY KEY (idcliente)
);
CREATE OR REPLACE TABLE productos ( 
  idproducto int,
  nombreproducto varchar (20),
  peso varchar (20),
  PRIMARY KEY (idproducto)

);

CREATE OR REPLACE TABLE telefono (
 
   idclien int,
   telefono varchar (20),
  PRIMARY KEY (idclien),
  CONSTRAINT fktelefonocliente FOREIGN KEY (idclien)
 REFERENCES clientes (idcliente)

);

CREATE OR REPLACE TABLE tienda (
  codigotienda int,
  direccion varchar (20),
  PRIMARY KEY (codigotienda)
  
);


CREATE OR REPLACE TABLE compran (
  idprodu int,
  idclien int,
  codtienda int,
    PRIMARY KEY (idprodu,idclien,codtienda),
  CONSTRAINT fkcompraclientes FOREIGN KEY (idclien)
  REFERENCES clientes (idcliente),
CONSTRAINT fkcompraproducto FOREIGN KEY (idprodu)
  REFERENCES productos (idproducto),
CONSTRAINT fkcompratienda FOREIGN KEY (codtienda)
  REFERENCES tienda (codigotienda)



 );