﻿DROP DATABASE IF EXISTS ejemplo5mod1;
CREATE DATABASE ejemplo5mod1;
USE ejemplo5mod1;



CREATE OR REPLACE TABLE productos ( 
  idproducto int,
  nombreproducto varchar (20),
  peso varchar (20),
  PRIMARY KEY (idproducto)
  
 
);


CREATE OR REPLACE TABLE clientes(
  idcliente int,
  nombre varchar (20),
  PRIMARY KEY (idcliente)
);
CREATE OR REPLACE TABLE productos ( 
  idproducto int,
  nombreproducto varchar (20),
  peso varchar (20),
  PRIMARY KEY (idproducto)

);

CREATE OR REPLACE TABLE telefono (
 
   idclien int,
   telefono varchar (20),
  PRIMARY KEY (idclien),
  CONSTRAINT fktelefonocliente FOREIGN KEY (idclien)
 REFERENCES clientes (idcliente)

);

CREATE OR REPLACE TABLE fecha (
 
    idpro int,
    idclie int,
    cantidad varchar (20),
   PRIMARY KEY (idpro,idclie),
 CONSTRAINT fkfechaproducto FOREIGN KEY (idpro)
  REFERENCES productos (idproducto),
CONSTRAINT fkclientes FOREIGN KEY (idclie)
  REFERENCES clientes (idcliente)



);


CREATE OR REPLACE TABLE compran (
  idprodu int,
  idclien int,
    PRIMARY KEY (idprodu,idclien),
  CONSTRAINT fkcompracliente FOREIGN KEY (idclien)
  REFERENCES clientes (idcliente),
CONSTRAINT fkcompraproducto FOREIGN KEY (idprodu)
  REFERENCES productos (idproducto)



 );