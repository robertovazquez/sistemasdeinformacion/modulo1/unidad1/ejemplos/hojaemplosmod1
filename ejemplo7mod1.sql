﻿DROP DATABASE IF EXISTS ejemplo7mod1;
CREATE DATABASE ejemplo7mod1;
USE ejemplo7mod1;


CREATE OR REPLACE TABLE productos (
   idproducto int,
   nombreproducto varchar (20),
   peso varchar (20),
  PRIMARY KEY  (idproducto) 

);

CREATE OR REPLACE TABLE clientes (

   idclientes int,
   nombrecliente varchar (20),
  PRIMARY KEY (idclientes)

);


CREATE OR REPLACE TABLE telefonos (
   idclien int,
   telefono varchar(20),
  PRIMARY KEY (idclien),
  CONSTRAINT fktelefonosclientes FOREIGN KEY (idclien)
  REFERENCES clientes (idclientes)

);

CREATE OR REPLACE TABLE tienda (

 codtienda int,
  direccion varchar (20),
  PRIMARY KEY (codtienda)


);


CREATE OR REPLACE TABLE compran (

idpro int,
  idclien int,
  codti int,

  PRIMARY KEY (idpro,idclien,codti),
  UNIQUE KEY (idpro,codti),
  CONSTRAINT fkcompranclientes FOREIGN KEY (idclien)
  REFERENCES clientes (idclientes),
  CONSTRAINT fkcompranproductos FOREIGN KEY (idpro)
  REFERENCES productos(idproducto),
  CONSTRAINT fkcomprantienda FOREIGN KEY (codti)
  REFERENCES tienda (codtienda)



);


CREATE OR REPLACE TABLE fechadatos(
  
  idprodu int,
  idclien int,
  codti int,
  fecha date,
  cantidad varchar (20),
 PRIMARY KEY (idprodu,idclien,codti),
  CONSTRAINT fkfechaclientes FOREIGN KEY (idclien)
  REFERENCES clientes (idclientes),
  CONSTRAINT fkfechaproductos FOREIGN KEY (idprodu)
  REFERENCES productos(idproducto),
  CONSTRAINT fkfechatienda FOREIGN KEY (codti)
  REFERENCES tienda (codtienda)

     
  );









