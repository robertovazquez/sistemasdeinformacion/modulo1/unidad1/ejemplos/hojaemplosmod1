﻿DROP DATABASE IF EXISTS ejemplo1mod1;
CREATE DATABASE ejemplo1mod1;
USE ejemplo1mod1;


CREATE OR REPLACE TABLE productos(
   idproducto int,
  nombre varchar (20),
  peso float,
  PRIMARY KEY (idproducto)


);

CREATE OR REPLACE TABLE clientes ( 
  idclientes int,
  nombreclientes varchar (20),
  PRIMARY KEY (idclientes)
  
 
);


CREATE OR REPLACE TABLE compran (
  idprodu int,
  idclien int,
  fecha date,
  PRIMARY KEY (idprodu,idclien),
  
CONSTRAINT fkcompraproducto FOREIGN KEY (idprodu)
  REFERENCES productos (idproducto)


  );